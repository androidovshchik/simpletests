package rf.androidovshchik.simpletests

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_start.*
import rf.androidovshchik.simpletests.data.Preferences

class StartActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        continue_old.setOnClickListener {
            startActivity(Intent(applicationContext, MainActivity::class.java))
        }
        start_new.setOnClickListener {
            preferences.putInt(Preferences.SAVED_POSITION, 0)
            preferences.putInt(Preferences.SAVED_POINTS, 0)
            startActivity(Intent(applicationContext, MainActivity::class.java))
        }
        others.setOnClickListener {
            try {
                startActivity(Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$packageName")))
            } catch (e: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
            }
        }
        help.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://vk.com/challenge")
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        continue_old.visibility = if (preferences.has(Preferences.SAVED_POSITION)) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
}
