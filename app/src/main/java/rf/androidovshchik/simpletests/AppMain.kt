package rf.androidovshchik.simpletests

import android.app.Application
import com.google.android.gms.ads.MobileAds
import timber.log.Timber

class AppMain: Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.BUILD_TYPE != "release") {
            Timber.plant(Timber.DebugTree())
        }
        MobileAds.initialize(applicationContext, getString(R.string.ads_app))
    }
}