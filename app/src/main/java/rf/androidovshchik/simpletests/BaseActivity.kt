package rf.androidovshchik.simpletests

import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import io.reactivex.disposables.CompositeDisposable
import rf.androidovshchik.simpletests.data.DbManager
import rf.androidovshchik.simpletests.data.Preferences
import timber.log.Timber

abstract class BaseActivity : AppCompatActivity() {

    protected val disposable: CompositeDisposable = CompositeDisposable()

    protected lateinit var dbManager: DbManager

    protected lateinit var preferences: Preferences

    private var progressDialog: ProgressDialog? = null

    protected var points: Int = 0

    protected var position: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        dbManager = DbManager(applicationContext)
        preferences = Preferences(applicationContext)
        position = preferences.getInt(Preferences.SAVED_POSITION, 0)
        points = preferences.getInt(Preferences.SAVED_POINTS, 0)
        Timber.d("Restored position: $position")
        Timber.d("Restored points: $points")
    }

    override fun onStart() {
        super.onStart()
        if (GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(applicationContext) != ConnectionResult.SUCCESS) {
            updateGMS()
        }
    }

    private fun updateGMS() {
        try {
            startActivity(Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + GoogleApiAvailability.GOOGLE_PLAY_SERVICES_PACKAGE)))
        } catch (e: Exception) {
            startActivity(Intent(Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=" + GoogleApiAvailability.GOOGLE_PLAY_SERVICES_PACKAGE)))
        }
    }

    protected fun showProgressDialog() {
        closeProgressDialog()
        progressDialog = ProgressDialog.show(this@BaseActivity, getString(R.string.wait),
            null, true)
    }

    protected fun closeProgressDialog() {
        if (progressDialog != null) {
            progressDialog!!.dismiss()
            progressDialog = null
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }

    companion object {

        fun getAdsRequest() : AdRequest {
            val adRequest = AdRequest.Builder()
            if (BuildConfig.BUILD_TYPE != "release") {
                adRequest.addTestDevice("FAA1BA6958CC85BA6B1B0483BE321991")
                adRequest.addTestDevice("BD1C60E379701FB989CE8D2BDBEE9501")
            }
            return adRequest.build()
        }
    }
}
