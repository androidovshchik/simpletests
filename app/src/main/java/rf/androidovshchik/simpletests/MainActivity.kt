package rf.androidovshchik.simpletests

import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import rf.androidovshchik.simpletests.data.Preferences
import rf.androidovshchik.simpletests.rows.Question
import timber.log.Timber

class MainActivity : BaseActivity() {

    private lateinit var rewardedVideoAd: RewardedVideoAd

    private var questions: ArrayList<Question> = ArrayList()

    private lateinit var questionAnim: ObjectAnimator

    private lateinit var answer1Anim: ObjectAnimator

    private lateinit var answer2Anim: ObjectAnimator

    private lateinit var answer3Anim: ObjectAnimator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        questionAnim = ObjectAnimator.ofFloat(questionText, "alpha", 0f, 1f)
        initAnimation(questionAnim)
        answer1Anim = ObjectAnimator.ofFloat(answer1, "alpha", 0f, 1f)
        initAnimation(answer1Anim)
        answer2Anim = ObjectAnimator.ofFloat(answer2, "alpha", 0f, 1f)
        initAnimation(answer2Anim)
        answer3Anim = ObjectAnimator.ofFloat(answer3, "alpha", 0f, 1f)
        initAnimation(answer3Anim)
        answer1.setOnClickListener {
            onAnswer(questions[position].points1)
        }
        answer2.setOnClickListener {
            onAnswer(questions[position].points2)
        }
        answer3.setOnClickListener {
            onAnswer(questions[position].points3)
        }
        adView.loadAd(getAdsRequest())
        disposable.add(dbManager.onSelectTable("SELECT rowid, * FROM questions")
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                it.use {
                    while (it.moveToNext()) {
                        val question = Question()
                        question.parseCursor(it)
                        questions.add(question)
                    }
                }
                onNextQuestion()
            })
        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(applicationContext)
        rewardedVideoAd.rewardedVideoAdListener = object : RewardedVideoAdListener {

            override fun onRewarded(reward: RewardItem) {
                Timber.d("onRewarded amount=${reward.amount} type=${reward.type}")
            }

            override fun onRewardedVideoAdLeftApplication() {
                Timber.d("onRewardedVideoAdLeftApplication")
            }

            override fun onRewardedVideoAdClosed() {
                Timber.d("onRewardedVideoAdClosed")
                openResult()
            }

            override fun onRewardedVideoAdFailedToLoad(errorCode: Int) {
                Timber.e("onRewardedVideoAdClosed errorCode=$errorCode")
                closeProgressDialog()
                openResult()
            }

            override fun onRewardedVideoAdLoaded() {
                Timber.d("onRewardedVideoAdLoaded")
                closeProgressDialog()
                rewardedVideoAd.show()
            }

            override fun onRewardedVideoAdOpened() {
                Timber.d("onRewardedVideoAdOpened")
            }

            override fun onRewardedVideoStarted() {
                Timber.d("onRewardedVideoStarted")
            }

            override fun onRewardedVideoCompleted() {
                Timber.d("onRewardedVideoCompleted")
            }
        }
    }

    private fun initAnimation(anim: ObjectAnimator) {
        anim.repeatCount = Animation.ABSOLUTE
        anim.interpolator = AccelerateInterpolator()
        anim.duration = 700
    }

    private fun onAnswer(newPoints: Int) {
        points += newPoints
        position++
        preferences.putInt(Preferences.SAVED_POSITION, position)
        preferences.putInt(Preferences.SAVED_POINTS, points)
        onNextQuestion()
    }

    private fun onNextQuestion() {
        Timber.d("On next position: $position")
        Timber.d("On next points: $points")
        if (position >= questions.size) {
            showProgressDialog()
            rewardedVideoAd.loadAd(getString(R.string.ads_reward), getAdsRequest())
        } else {
            val question = questions[position]
            questionText.text = question.question
            questionAnim.start()
            if (question.answer1 != null) {
                answer1.text = question.answer1
                answer1.visibility = View.VISIBLE
                answer1Anim.start()
            } else {
                answer1.visibility = View.GONE
            }
            if (question.answer2 != null) {
                answer2.text = question.answer2
                answer2.visibility = View.VISIBLE
                answer2Anim.start()
            } else {
                answer2.visibility = View.GONE
            }
            if (question.answer3 != null) {
                answer3.text = question.answer3
                answer3.visibility = View.VISIBLE
                answer3Anim.start()
            } else {
                answer3.visibility = View.GONE
            }
            count.text = getString(R.string.position, position + 1, questions.size)
            count.visibility = View.VISIBLE
        }
    }

    private fun openResult() {
        val intent = Intent(applicationContext, ResultActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    override fun onResume() {
        super.onResume()
        rewardedVideoAd.resume(applicationContext)
    }

    override fun onPause() {
        super.onPause()
        rewardedVideoAd.pause(applicationContext)
    }

    override fun onDestroy() {
        super.onDestroy()
        rewardedVideoAd.destroy(applicationContext)
    }
}
