package rf.androidovshchik.simpletests

import android.os.Bundle
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_result.*
import rf.androidovshchik.simpletests.data.Preferences
import rf.androidovshchik.simpletests.rows.Result
import timber.log.Timber

class ResultActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        preferences.remove(Preferences.SAVED_POSITION)
        preferences.remove(Preferences.SAVED_POINTS)
        disposable.add(dbManager.onSelectTable("SELECT rowid, * FROM results")
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                var text = getString(R.string.unknown_result)
                it.use {
                    while (it.moveToNext()) {
                        val result = Result()
                        result.parseCursor(it)
                        val realMin = Math.min(result.min, result.max)
                        val realMax = Math.max(result.min, result.max)
                        Timber.d("Next min: $realMin max: $realMax")
                        if (points in realMin..realMax) {
                            text = result.result
                            break
                        }
                    }
                }
                resultText.text = text
            })
        go_main.setOnClickListener {
            finish()
        }
    }
}
