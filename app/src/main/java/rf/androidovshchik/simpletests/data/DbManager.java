package rf.androidovshchik.simpletests.data;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Factory;
import android.arch.persistence.db.framework.FrameworkSQLiteOpenHelperFactory;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.squareup.sqlbrite3.BriteDatabase;
import com.squareup.sqlbrite3.SqlBrite;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.simpletests.BuildConfig;

public class DbManager {

    public BriteDatabase db;

    @SuppressWarnings("all")
    public DbManager(Context context) {
        DbCallback dbCallback = new DbCallback();
        dbCallback.openDatabase(context);
        Configuration configuration = Configuration.builder(context)
            .name(DbCallback.DATABASE_NAME)
            .callback(dbCallback)
            .build();
        Factory factory = new FrameworkSQLiteOpenHelperFactory();
        SupportSQLiteOpenHelper openHelper = factory.create(configuration);
        db = new SqlBrite.Builder()
            .logger(new SqlBrite.Logger() {
                @Override
                public void log(String message) {
                    Log.v(getClass().getSimpleName(), message);
                }
            })
            .build()
            .wrapDatabaseHelper(openHelper, Schedulers.io());
        db.setLoggingEnabled(BuildConfig.DEBUG);
    }

    public Observable<Cursor> onSelectTable(final String sql) {
        return Observable.create(new ObservableOnSubscribe<Cursor>() {

            @Override
            public void subscribe(ObservableEmitter<Cursor> emitter) {
                if (emitter.isDisposed()) {
                    return;
                }
                Cursor cursor;
                BriteDatabase.Transaction transaction = db.newTransaction();
                try {
                    cursor = db.query(sql);
                    transaction.markSuccessful();
                } finally {
                    transaction.end();
                }
                if (cursor != null) {
                    emitter.onNext(cursor);
                }
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }
}
