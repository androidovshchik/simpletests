package rf.androidovshchik.simpletests.rows;

import android.content.ContentValues;
import android.database.Cursor;

public class Result extends Row {

	public static final String TABLE = "results";

	private static final String COLUMN_RESULT = "result";
	private static final String COLUMN_MIN = "min";
	private static final String COLUMN_MAX = "max";

	public String result;

	public int min;

	public int max;

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (rowId != NONE) {
			values.put(COLUMN_ROW_ID, rowId);
		}
		values.put(COLUMN_RESULT, result);
		values.put(COLUMN_MIN, min);
		values.put(COLUMN_MAX, max);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		rowId = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ROW_ID));
		result = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_RESULT));
		min = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_MIN));
		max = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_MAX));
	}

	@Override
	public String getTable() {
		return TABLE;
	}
}
