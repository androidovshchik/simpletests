package rf.androidovshchik.simpletests.rows;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class Question extends Row {

	public static final String TABLE = "questions";

	private static final String COLUMN_QUESTION = "question";
	private static final String COLUMN_ANSWER1 = "answer1";
	private static final String COLUMN_ANSWER2 = "answer2";
	private static final String COLUMN_ANSWER3 = "answer3";
	private static final String COLUMN_POINTS1 = "points1";
	private static final String COLUMN_POINTS2 = "points2";
	private static final String COLUMN_POINTS3 = "points3";

	public String question;

	@Nullable
	public String answer1;

	@Nullable
	public String answer2;

	@Nullable
	public String answer3;

	public int points1;

	public int points2;

	public int points3;

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (rowId != NONE) {
			values.put(COLUMN_ROW_ID, rowId);
		}
		values.put(COLUMN_QUESTION, question);
		values.put(COLUMN_ANSWER1, answer1);
		values.put(COLUMN_ANSWER2, answer2);
		values.put(COLUMN_ANSWER3, answer3);
		values.put(COLUMN_POINTS1, points1);
		values.put(COLUMN_POINTS2, points2);
		values.put(COLUMN_POINTS3, points3);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		rowId = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ROW_ID));
		question = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_QUESTION));
		answer1 = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ANSWER1));
		if (TextUtils.isEmpty(answer1)) {
			answer1 = null;
		}
		answer2 = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ANSWER2));
		if (TextUtils.isEmpty(answer2)) {
			answer2 = null;
		}
		answer3 = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ANSWER3));
		if (TextUtils.isEmpty(answer3)) {
			answer3 = null;
		}
		points1 = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_POINTS1));
		points2 = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_POINTS2));
		points3 = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_POINTS3));
	}

	@Override
	public String getTable() {
		return TABLE;
	}
}
